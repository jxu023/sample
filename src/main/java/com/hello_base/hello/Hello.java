package com.hello_base.hello;

/**
 * Hello world!
 *
 */
public class Hello 
{
	private String hello;
	
	public Hello () {
		hello = "Hello";
	}
	
	public String get_Greeting() {
		return hello;
	}
}
