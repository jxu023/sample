Feature: get_Greeting() test
	Just a simple feature
	
	Scenario: Instantiate hello and call get_Greeting
		Given That I instantiate Hello
		When I call get_Greeting
		Then I should receive the string Hello
		
	Scenario: Use JUnit test here
		http://stackoverflow.com/questions/2543912/how-do-i-run-junit-tests-from-inside-my-java-application
		
		Given I have a JUnit test I already wrote called Test2Hello.java
		When I Call the JUnit test from here
		Then I'll get some test result here too
	
	Scenario: This here is just to sandwich see that it continues executing
		Given get a greetingggggg