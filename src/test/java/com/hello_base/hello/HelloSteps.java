package com.hello_base.hello;

import cucumber.api.java.en.*;
import cucumber.api.PendingException;
import static org.junit.Assert.*;

import org.junit.runner.JUnitCore;
import org.junit.runner.Result;

public class HelloSteps {
	Hello hello;
	String str;
	@Given("^That I instantiate Hello$")
	public void that_I_instantiate_Hello() throws Throwable {
		hello = new Hello();
		assertNotNull(hello);
	}

	@When("^I call get_Greeting$")
	public void i_call_get_Greeting() throws Throwable {
		str = hello.get_Greeting();
	//	assertNotNull(null);
	}

	@Then("^I should receive the string Hello$")
	public void i_should_receive_the_string_Hello() throws Throwable {
		assertEquals(str,"Hello");
	}
	
	// http://stackoverflow.com/questions/2543912/how-do-i-run-junit-tests-from-inside-my-java-application
	@Given("^I have a JUnit test I already wrote called Test(\\d+)Hello\\.java$")
	public void i_have_a_JUnit_test_I_already_wrote_called_Test_Hello_java(int arg1) throws Throwable {

	}

	// JUnit Core can probably be instantiated once a HelloSteps member
	// and then reused.
	// Perhaps pass the JUnit test class name as an argument to here from 
	// the feature file, only really useful with automatically generated
	// step definitions though
	@When("^I Call the JUnit test from here$")
	public void i_Call_the_JUnit_test_from_here() throws Throwable {
		JUnitCore junit = new JUnitCore();
		Result result = junit.run(Test2Hello.class);
		result.
		assertTrue(result.getFailureCount() == 0);
	}

	@Then("^I'll get some test result here too$")
	public void i_ll_get_some_test_result_here_too() throws Throwable {
	}
	
	@Given("^get a greetingggggg$")
	public void get_a_greetingggggg() throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
	    throw new PendingException();
	}
}
