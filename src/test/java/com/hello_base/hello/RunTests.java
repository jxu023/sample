package com.hello_base.hello;

import org.junit.runner.RunWith;
import cucumber.api.junit.Cucumber;
import cucumber.api.CucumberOptions;
import junit.framework.TestCase;

@RunWith(Cucumber.class)
@CucumberOptions(features = "src/test/features",
				 glue = "com/hello_base/hello",
				 monochrome = true,
				 plugin = {"pretty","html:target/"})
// if features is not set then default location is in
// directory: src/test/resources/package
public class RunTests extends TestCase {
	
}