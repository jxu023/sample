$(document).ready(function() {var formatter = new CucumberHTML.DOMFormatter($('.cucumber-report'));formatter.uri("hello.feature");
formatter.feature({
  "id": "get-greeting()-test",
  "description": "Just a simple feature",
  "name": "get_Greeting() test",
  "keyword": "Feature",
  "line": 1
});
formatter.scenario({
  "id": "get-greeting()-test;instantiate-hello-and-call-get-greeting",
  "description": "",
  "name": "Instantiate hello and call get_Greeting",
  "keyword": "Scenario",
  "line": 4,
  "type": "scenario"
});
formatter.step({
  "name": "That I instantiate Hello",
  "keyword": "Given ",
  "line": 5
});
formatter.step({
  "name": "I call get_Greeting",
  "keyword": "When ",
  "line": 6
});
formatter.step({
  "name": "I should receive the string Hello",
  "keyword": "Then ",
  "line": 7
});
formatter.match({
  "location": "HelloSteps.that_I_instantiate_Hello()"
});
formatter.result({
  "duration": 201842598,
  "status": "passed"
});
formatter.match({
  "location": "HelloSteps.i_call_get_Greeting()"
});
formatter.result({
  "duration": 314613,
  "status": "failed",
  "error_message": "java.lang.AssertionError\r\n\tat org.junit.Assert.fail(Assert.java:86)\r\n\tat org.junit.Assert.assertTrue(Assert.java:41)\r\n\tat org.junit.Assert.assertNotNull(Assert.java:712)\r\n\tat org.junit.Assert.assertNotNull(Assert.java:722)\r\n\tat com.hello_base.hello.HelloSteps.i_call_get_Greeting(HelloSteps.java:19)\r\n\tat ✽.When I call get_Greeting(hello.feature:6)\r\n"
});
formatter.match({
  "location": "HelloSteps.i_should_receive_the_string_Hello()"
});
formatter.result({
  "status": "skipped"
});
});